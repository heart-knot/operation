package utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Xinjie
 * @date 2023/9/25 11:26
 */
public class FileUtils {
    public static void writeTA(String name, String[] strings) throws IOException {
        FileWriter fw;
        //题目写入
        File f = new File(name + ".txt");
        fw = new FileWriter(f, true);
        int i = 0;
        PrintWriter pw = new PrintWriter(fw);
        for (String string : strings) {
            pw.println(i + 1 + ". " + string);
            pw.flush();
            i += 1;
        }
        fw.flush();
        pw.close();
        fw.close();
    }

    public static void writeG(int[] rightCount, int right, int[] wrongCount, int wrong) throws IOException {
        FileWriter fw;
        File f = new File("Grade.txt");
        fw = new FileWriter(f, true);
        PrintWriter pw = new PrintWriter(fw);
        pw.println(" ");
        pw.print("Correct:" + right + "(");
        for (int i = 0; i < rightCount.length; i++) {
            if (rightCount[i] != 0 && i != rightCount.length - 1) {
                pw.print(rightCount[i] + ",");
            } else if (rightCount[i] != 0 && i == rightCount.length - 1) {
                pw.print(rightCount[i]);
            }
        }
        pw.println(")");
        pw.print("Wrong:" + wrong + "(");
        for (int j = 0; j < wrongCount.length; j++) {
            if (wrongCount[j] != 0 && j != wrongCount.length - 1) {
                pw.print(wrongCount[j] + ",");
            } else if (wrongCount[j] != 0 && j == wrongCount.length - 1) {
                pw.print(wrongCount[j]);
            }
        }
        pw.print(")");
        pw.flush();
        fw.flush();
        pw.close();
        fw.close();
    }
}
