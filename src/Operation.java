import utils.FileUtils;
import utils.TopicUtils;

import java.io.IOException;
import java.util.Map;
import java.util.Scanner;

/**
 * @author Xinjie
 * @date 2023/9/25 11:21
 */
public class Operation {
    public static void main(String[] args) throws IOException {
        //输入
        Scanner scanner = new Scanner(System.in);
        //题目数量
        System.out.println("请输入题目数量：");
        int num = scanner.nextInt();
        //数值范围
        System.out.println("请输入题目数值范围：");
        int range = scanner.nextInt();
        Map<String, String[]> map = TopicUtils.generateTopic(num, range);
        FileUtils.writeTA("Exercises", map.get("topics"));
        FileUtils.writeTA("Answers", map.get("answers"));
        String[] topics = map.get("topics");
        String[] answers = map.get("answers");
        String[] results = new String[topics.length];
        for (int i = 0; i < topics.length; i++) {
            int j = i + 1;
            System.out.println("题目" + j + ": " + topics[i]);
            System.out.println("请输入你的答案:");
            results[i] = scanner.next();
        }
        int[] rightCount = new int[results.length];
        int right = 0;
        int[] wrongCount = new int[results.length];
        int wrong = 0;
        for (int i = 0; i < results.length; i++) {
            if (results[i].equals(answers[i])) {
                rightCount[i] = i + 1;
                right++;
            } else {
                wrongCount[i] = i + 1;
                wrong++;
            }
        }
        FileUtils.writeG(rightCount, right, wrongCount, wrong);
    }
}
